#include<stdio.h>

struct Employee1
{
long int salary;
char name[20];
int Id;
};

union Employee2
{
long int salary;
char name[20];
int Id;
};

int main()
{
 printf(" Size of long int  : %d\n",sizeof(long int));
 printf(" Size of int       : %d\n",sizeof(int));
 printf(" Size of name      : %d\n",20*sizeof(char));
 printf(" Size of structure : %d",sizeof(long int) + sizeof(int) + 20*sizeof(char));
 printf("\n Size of union     : %d\n",sizeof(union Employee2));
 return 0;
}
