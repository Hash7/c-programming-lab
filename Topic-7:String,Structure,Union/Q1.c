#include <stdio.h>

int main()
{
    char text[100];
    char * st = text;
    int alphabets, digits, others;

    alphabets = digits = others = 0;

    printf("Enter any string : ");
    fgets(text,100,stdin);

    while(*st)
    {
        if((*st >= 'a' && *st <= 'z') || (*st >= 'A' && *st <= 'Z'))
            alphabets++;
        else if(*st>='0' && *st<='9')
            digits++;
        else
            others++;

        st++;
    }

    printf("\n Number of Alphabets = %d", alphabets);
    printf("\n Number of Digits = %d", digits);
    printf("\n Number of Special characters = %d", others);

    return 0;
}
