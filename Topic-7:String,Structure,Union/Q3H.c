#include<stdio.h>
#include<string.h>
struct TIME {
    char hours[5];
    char minutes[5];
};
struct TRAIN_INFO {
    int tr_no;
    char tr_name[20];
    char tr_station1[20],tr_station2[20];
    struct TIME t1,t2;
};

int main()
{
    struct TRAIN_INFO TR[10];
    for(int i=0; i<10; i++) 
    {
        TR[i].tr_no = i+1;
    }
    printf("  **TRAIN TIME TABLE**\n");
    strcpy(TR[0].tr_name,"Shatabdi Express");
    strcpy(TR[1].tr_name,"Gujrat Express");
    strcpy(TR[2].tr_name,"khajan Mail Express");
    strcpy(TR[3].tr_name,"Rajdhani Express");
    strcpy(TR[4].tr_name,"Maharashtra Express");
    strcpy(TR[5].tr_name,"Jaipur Express");
    strcpy(TR[6].tr_name,"Duronto Express");
    strcpy(TR[7].tr_name,"Vande Bharat Express");
    strcpy(TR[8].tr_name,"Sampoorna Express ");
    strcpy(TR[9].tr_name,"Delhi Express");
    strcpy(TR[0].tr_station1,"Allahabad Junction");
    strcpy(TR[1].tr_station1,"Gujrat Junction");
    strcpy(TR[2].tr_station1,"Allahabad Junction");
    strcpy(TR[3].tr_station1,"Delhi Junction");
    strcpy(TR[4].tr_station1,"Mumbai Junction");
    strcpy(TR[5].tr_station1,"jaipur Junction");
    strcpy(TR[6].tr_station1,"jaipur Junction");
    strcpy(TR[7].tr_station1,"Delhi Junction");
    strcpy(TR[8].tr_station1,"Gujrat Junction");
    strcpy(TR[9].tr_station1,"Naseerabad Junction");
    strcpy(TR[0].tr_station2,"Bopal Junction");
    strcpy(TR[1].tr_station2,"Delhi Junction");
    strcpy(TR[2].tr_station2,"Ujain Junction");
    strcpy(TR[3].tr_station2,"jhodpur Junction");
    strcpy(TR[4].tr_station2,"Delhi Junction");
    strcpy(TR[5].tr_station2,"kota Junction");
    strcpy(TR[6].tr_station2,"Bopal Junction");
    strcpy(TR[7].tr_station2,"Mumbai Junction");
    strcpy(TR[8].tr_station2,"jhodpur Junction");
    strcpy(TR[9].tr_station2,"Allahabad Junction");
    strcpy(TR[0].t1.hours,"12:");
    strcpy(TR[0].t1.minutes,"30");
    strcpy(TR[1].t1.hours,"14:");
    strcpy(TR[1].t1.minutes,"00");
    strcpy(TR[2].t1.hours,"15:");
    strcpy(TR[2].t1.minutes,"30");
    strcpy(TR[3].t1.hours,"14:");
    strcpy(TR[3].t1.minutes,"00");
    strcpy(TR[4].t1.hours,"22:");
    strcpy(TR[4].t1.minutes,"45");
    strcpy(TR[5].t1.hours,"22:");
    strcpy(TR[5].t1.minutes,"45");
    strcpy(TR[6].t1.hours,"14:");
    strcpy(TR[6].t1.minutes,"00");
    strcpy(TR[7].t1.hours,"12:");
    strcpy(TR[7].t1.minutes,"00");
    strcpy(TR[8].t1.hours,"15:");
    strcpy(TR[8].t1.minutes,"30");
    strcpy(TR[9].t1.hours,"14:");
    strcpy(TR[9].t1.minutes,"00");
    strcpy(TR[0].t2.hours,"22:");
    strcpy(TR[0].t2.minutes,"45");
    strcpy(TR[1].t2.hours,"24:");
    strcpy(TR[1].t2.minutes,"00");
    strcpy(TR[2].t2.hours,"24:");
    strcpy(TR[2].t2.minutes,"30");
    strcpy(TR[3].t2.hours,"23:");
    strcpy(TR[3].t2.minutes,"00");
    strcpy(TR[4].t2.hours,"06:");
    strcpy(TR[4].t2.minutes,"45");
    strcpy(TR[5].t2.hours,"06:");
    strcpy(TR[5].t2.minutes,"45");
    strcpy(TR[6].t2.hours,"24:");
    strcpy(TR[6].t2.minutes,"00");
    strcpy(TR[7].t2.hours,"24:");
    strcpy(TR[7].t2.minutes,"30");
    strcpy(TR[8].t2.hours,"22:");
    strcpy(TR[8].t2.minutes,"00");
    strcpy(TR[9].t2.hours,"22:");
    strcpy(TR[9].t2.minutes,"00");
   
    for(int i=0; i<10; i++)
    {
    printf("\t(%d)\nTrain no. : %d\nTrain name : %s\nTrain Arrival time : %s%s\nTrain departure time : %s%s\nStart station : %s\nEnd station : %s\n\n",i+1,TR[i].tr_no,TR[i].tr_name,TR[i].t1.hours,TR[i].t1.minutes,TR[i].t2.hours,TR[i].t2.minutes,TR[i].tr_station1,TR[i].tr_station2);
    }
   
    char str[15],str1[15],str2[15];
    int k=0;
    printf("Enter station name:");
    fgets(str,15,stdin);
    printf("\nTrains departing from : %s\n",str);
   
    for(int i=0; i<10; i++)
    {
      if(strcmp(str,TR[i].tr_station1)==0)
      printf("%d) %s\n",++k,TR[i].tr_name);
    }
    k=0;
    printf("Enter departing station name :");
    fgets(str1,15,stdin);
    printf("Enter Ending station name :");
    fgets(str2,15,stdin);
    printf("\nTrains departing from : %s and Ending to : %s\n",str1,str2);
   
    for(int i=0; i<10; i++)
    {
    if(strcmp(str1,TR[i].tr_station1)==0 && strcmp(str2,TR[i].tr_station2)==0)
    printf("%d) %s\n",++k,TR[i].tr_name);
    }
    k=0;
    return 0;
}
