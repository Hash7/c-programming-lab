#include<stdio.h>

struct DATE
{
  int day;
  int month;
  int year;
};

int Isvalid(struct DATE var) 
{
  if(var.day>31 || var.day<1)
  return 0;

  else
  {
    if(var.month>12 || var.month<1)
    return 0;

    else 
    {
      if((var.day==31 && var.month==2)||(var.day==31 && var.month==6)||(var.day==30 && var.month==2)||(var.day==31       && var.month==4)||(var.day==31 && var.month==11)||(var.day==31 && var.month==9))
      return 0;

      else
      {
        if(var.year<1000)
        return 0;
        return 1;
      }
    }
  }
}

struct DATE Nextdate(struct DATE var) 
{
  if(var.day==29 && var.month==2) 
  {
    var.day = 1;
    var.month = 3;
    return var;
  }
  if(var.day<30) 
  {
    var.day += 1;
    return var;
  }
  else
  {
    if((var.day==30 && var.month==4)||(var.day==30 && var.month==6)||(var.day==30 && var.month==9)||(var.day==30 &&
        var.month==11)) 
    {
       var.day = 1;
       var.month += 1;
       return var;
    }
    else if((var.day==30 && var.month==1)||(var.day==30 && var.month==3)||(var.day==30 && var.month==5)||(var.day==30 && var.month==7)||(var.day==30 && var.month==8)||(var.day==30 && var.month==10)||(var.day==30 && var.month==12)) 
    {
      var.day += 1;
      return var;
    }
    else
    {
      if(var.month==12) 
      {
          var.day = 1;
          var.month = 1;
          var.year += 1;
          return var;
      }
      else
      { 
          var.day = 1; 
          var.month += 1;
          return var;
      }
    }
  }
}

struct DATE datediff(struct DATE var,struct DATE nextvar) 
{
  struct DATE diff;
  if(var.month == nextvar.month && var.year == nextvar.year) 
  {
      diff.day = var.day - nextvar.day;
      diff.month = var.month - nextvar.month;
      diff.year = var.year - nextvar.year;
      
      if(diff.day<0)
          diff.day = - diff.day;
      return (diff);
  }

  if(var.month != nextvar.month) 
  {
      diff.day = (var.day - nextvar.day)%30;
      diff.month = (var.month - nextvar.month);
      diff.year = var.year - nextvar.year;
      return diff;
  }
}

int main()
{

    struct DATE

    var,nextvar,diff;
    printf("Enter any date in (dd/mm/yyyy):");
    scanf("%d%*c%d%*c%d",&var.day,&var.month,&var.year);
    printf("\nEntered Date : %02d-%02d -%d\n",var.day,var.month,var.year);

    if(Isvalid(var))
    {
        printf("Entered date is valid");
        nextvar = Nextdate(var);
        printf("\nNext Date : %02d-%02d-%d\n",nextvar.day,nextvar.month,nextvar.year);
    }
    else
        printf("Entered date is not valid");
        printf("\nEnter first date in (dd/mm/yyyy):");
        scanf("%d%*c%d%*c%d",&var.day,&var.month,&var.year);
        printf("\nEnter second date in (dd/mm/yyyy):");
        scanf("%d%*c%d%*c%d",&nextvar.day,&nextvar.month,&nextvar.year);
        diff = datediff(var,nextvar);
        printf("\ndifference Date : %02d days,%02d months,%02d years\n",diff.day,diff.month,diff.year);

        return 0;
}
