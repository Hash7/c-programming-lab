#include <stdio.h>
#include <math.h>

int Armstrong(int Num)
{
int Temp, Reminder, Times = 0, Sum = 0;
Temp = Num;

while (Temp != 0)
{
Times = Times + 1;
     Temp = Temp / 10;
 }

  for(Temp = Num; Temp > 0; Temp =  Temp /10 )
   {
    Reminder = Temp % 10;
    Sum = (Sum + pow(Reminder, Times));
   }
  if ( Num == Sum )
  return 1;
  else
 return 0;
}

int PerfectNumber(int Num)
{
int i, Sum = 0 ;

 for(i = 1 ; i < Num ; i++)
  {
  if(Num % i == 0)
    Sum = Sum + i ;
  }

 if (Sum == Num)
    return 1;
 else
    return 0;
}

int PrimeNumber(int Num)
{
int i, Count = 0;

for (i = 2; i <= Num/2; i++)
   {
    if(Num%i == 0)
     {
       Count++;
   }
    }
   if(Count == 0 && Num != 1 )
   return 1;
   else
   return 0;
}

int main()
{
  int Num;

  printf("\nPlease Enter Number to Check whether it is an Armstrong, Prime, or Perfect :  ");
  scanf("%d", &Num);

  if (Armstrong(Num))
    printf("\n %d is an Armstrong Number.", Num);
  else
    printf("\n %d is Not an Armstrong Number.", Num);

  if(PrimeNumber(Num) )
   printf("\n %d is a Prime Number", Num);
  else
   printf("\n %d is Not a Prime Number", Num);

  if (PerfectNumber(Num) )
    printf("\n %d is a Perfect Number", Num) ;
  else
    printf("\n %d is Not a Perfect Number", Num) ;

 return 0;
}
