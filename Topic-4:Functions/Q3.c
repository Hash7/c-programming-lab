
#include<stdio.h>

void checkeven(int N , int *c){
    int count=*c;
    if(N%2==0){
        count++;
        *c=count;
    }
}

int main(){
    int n;
    printf("Number of terms to be checked: ");
    scanf("%d",&n);
    printf("Enter the numbers: ");
    int counter=0;
    while(n--){
        int N;
        scanf("%d",&N);
        checkeven(N,&counter);}
    printf("Number of even terms= %d",counter);
    return 0;}
