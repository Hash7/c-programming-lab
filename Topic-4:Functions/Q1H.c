#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int isStrong(int num){
 int digit, sum=0, i, fact=1;
 int temp = num;
 while(temp>0){
 digit = temp%10;
 fact=1;
 for(i=1; i<=digit; i++){
 fact = fact*i;
 }

 sum += fact;
 temp = temp/10;
 }
 return (sum == num);
}
int main()
{
 int end, i;
 char c;

 do
 {
 printf("Enter upper Interval Value \n");
 scanf("%d",&end);

 for(i=1; i<=end; i++)
 {
 if(isStrong(i))
 printf("%d \n",i);
 }
 printf("\n enter y/Y to continue\n");
 scanf("%s",&c);

 }while(c=='y'||c=='Y');

 return 0;
}
