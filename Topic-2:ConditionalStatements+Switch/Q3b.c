#include<stdio.h>
int main()
{
 char ch;
 printf("\n Enter Any Character :");
 scanf("%c",&ch);

 if(ch>='0' && ch<='9')
 {
     printf("\n Entered Character is Digit");

     if(ch%2==0 && ch%5==0){
         printf("\n %c is divisible by 2 and 5 ",ch);
     }
     else {
         printf("\n %c is not divisible by 2 and 5 ",ch);
     }
 }

 else if((ch>='A' && ch<='Z')||(ch>='a' && ch<='z'))
 {
     printf("\n Entered Character is an \n * Alphabet");

     if(ch>='A' && ch<='Z') {
         printf("\n * Capital Letter");
     }

     else if(ch>='a' && ch<='z') {

         printf("\n * Small Letter");

         int vowel;
         vowel = (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u');

         if (vowel) { printf("\n * a vowel.");    }
         else       { printf("\n * a consonant.");}

     }
  return 0;
 }
}
