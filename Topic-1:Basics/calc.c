#include <stdio.h>
int main()
{
    int a, b;
    int add, sub, mult, mod;
    float div;

    printf("Input any two numbers : ");
    scanf("%d %d", &a, &b);

    add = a + b;
    sub = a - b;
    mult= a * b;
    div = a / b;
    mod = a % b;

    printf("The sum of the given numbers : %d\n", add);
    printf("The difference of the given numbers : %d\n", sub);
    printf("The product of the given numbers : %d\n", mult);
    printf("The quotient of the given numbers : %f\n", div);
    printf("MODULUS = %d\n", mod);

    return 0;
}
