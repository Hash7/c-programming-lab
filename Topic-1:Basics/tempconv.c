#include <stdio.h>
int main()
{
    float C, F;
    int n;

    printf(" Type 1 for °C to °F Conversion \n");
    printf(" Type 2 for °F to °C Conversion \n\n");
    printf(" Option : ");
    scanf("%d", &n);

    switch (n) {

        case 1:  printf("Enter temperature in °C: ");
                 scanf("%f", &C);
                 F = (C * 9 / 5) + 32;
                 printf("%.2f °C = %.2f °F", C, F);

                 break;

        case 2:  printf("Enter temperature in °F: ");
                 scanf("%f", &F);
                 C = (F * 5 / 9) + 17.77;
                 printf("%.2f °F = %.2f °C", F, C);

                 break;

        default: printf("Wrong input");

                 break;
    }

    return 0;
}
